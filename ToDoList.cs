﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV1
{
    class ToDoList
    {
        //task no. 7 (first part):
        private List<Note> notes;
        public ToDoList()
        {
            notes = new List<Note>();
        }
        public int AddNote(string text, string author, int importanceLevel)
        {
            Note toDoNote = new Note(text, author, importanceLevel);
            notes.Add(toDoNote);
            return notes.IndexOf(toDoNote);
        }

        public void RemoveNote(int index)
        {
            notes.RemoveAt(index);
        }

        public Note GetNote(int index)
        {
            return notes[index];
        }

        public override string ToString()
        {
            string notes= string.Empty;
            foreach (Note toDoNote in this.notes)
            {
                notes += toDoNote.ToString() + "  ";
            }
            return notes;
        }

        private int GetTheMostImportantLevel()
        {
            int mostImportantLevel = notes[0].ImportanceLevel;

            foreach (Note toDoNote in notes)
            {
                if (mostImportantLevel > toDoNote.ImportanceLevel)
                {
                    mostImportantLevel = toDoNote.ImportanceLevel;
                }
            }

            return mostImportantLevel;
        }

        public void RemoveTheMostImportantNotes()
        {
            int mostImportantLevel = GetTheMostImportantLevel();

            for (int i = 0; i < notes.Count; i++)
            {
                if (mostImportantLevel == notes[i].ImportanceLevel)
                {
                    notes.RemoveAt(i);
                    i--;
                }
            }
        }   
    }
}
