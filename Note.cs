﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV1
{   
    class Note 
    {
        //task no. 2 (task no. 1 was to create this project):
        private String text;
        private String author;
        private int importanceLevel;

        public string GetText() { return this.text; }
        public void SetText(string text) { this.text = text; }
        public int GetImportanceLevel() { return this.importanceLevel; } 
        public void SetImportanceLevel(int importanceLevel) { this.importanceLevel = importanceLevel; }
        public string GetAuthor() { return this.author; }
        private void SetAuthor(string author) { this.author = author; }

        //task no. 3 (first part):
        public Note()
        {
            this.text = "Text of the note.";
            this.author = "Name of the author.";
            this.importanceLevel = 0;
        }

        public Note(string text, string author)
        {
            this.text = text;
            this.author = author;
            this.importanceLevel = 0;
        }

        public Note(string text, string author, int importanceLevel)
        {
            this.text = text;
            this.author = author;
            this.importanceLevel = importanceLevel;
        }

        //task no. 4:
        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public string Author
        {
            get { return this.author; }
            private set { this.author = value; }
        }
        public int ImportanceLevel
        {
            get { return this.importanceLevel; }
            set { this.importanceLevel = value; } 
        }

        //task no. 5:
        public override string ToString()
        {
            return this.Text + ", " + this.Author + ", " + this.ImportanceLevel;
        }
    }
}
