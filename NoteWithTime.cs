﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV1
{
    //task no.6:
    class NoteWithTime:Note
    {
        private DateTime time = new DateTime();
        public NoteWithTime() : base()
        {
            time = DateTime.Now;
        }
        public NoteWithTime(string text, string author, int importanceLevel, DateTime time)
            : base(text, author, importanceLevel)
        {
            this.time = time;
        }

        public DateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }

        public override string ToString()
        {
            return base.ToString() + ", " + Time;
        }
    }
}
