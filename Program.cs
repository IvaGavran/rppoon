﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            int size = 3;
            //task no. 3 (second part):
            Note firstNote;
            firstNote = new Note();

            Console.WriteLine(firstNote.GetAuthor() + " " + firstNote.GetText() + "\n");

            Note secondNote;
            secondNote = new Note("Second note.", "Iva");

            Console.WriteLine(secondNote.GetAuthor() + " " + secondNote.GetText() + "\n");

            Note thirdNote;
            thirdNote = new Note("Third note.", "Iva", 1);

            Console.WriteLine(thirdNote.GetAuthor() + " " + thirdNote.GetText() + "\n");

            //task no. 7 (second part):
            ToDoList todoList = new ToDoList();

            for (int i = 0; i < size; i++)
            {
                todoList.AddNote(Console.ReadLine(), Console.ReadLine(), Convert.ToInt32(Console.ReadLine()));
            }

            Console.WriteLine(todoList.ToString());

            todoList.RemoveTheMostImportantNotes();

            Console.WriteLine(todoList.ToString());

        }
    }
}
